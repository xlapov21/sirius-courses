import os
from dotenv import load_dotenv
import i18n  # pip install python-i18n
from datetime import datetime
import re
import random
from sentence_transformers import SentenceTransformer, util
import sqlite3 as sl
import telebot

MY_REG = r'^test (.*)$'
def get_env_var(var_name):
    load_dotenv()
    return os.getenv(var_name)




def get_text(address, locale):
    i18n.load_path.append('locales')
    return i18n.t(address, locale=locale)


def log(text):
    file_log = open('message_log.txt', 'a')
    file_log.write(f'{str(datetime.now())}.  text: {text}\n')


if os.path.isfile('./my-test.db'):
    os.remove('./my-test.db')
con = sl.connect('my-test.db')
with con:
    con.execute("""
        CREATE TABLE USER (
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            name TEXT,
            age INTEGER
        );
    """)

log('test text')
model = SentenceTransformer('all-MiniLM-L6-v2')

print(get_env_var('TEST'))
print(get_text('message.test', 'ru'))
print(get_text('message.test', 'en'))
print(re.search(MY_REG, 'test regular successful')[1])
