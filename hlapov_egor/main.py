
# db_init()
from lib.env_var_selector import get_env_var
import telebot
import hlapov_egor.app.controllers.general_controller as t

token = get_env_var('BOT_TOKEN')
bot = telebot.TeleBot(token)

t.register_command(bot)

bot.infinity_polling()
