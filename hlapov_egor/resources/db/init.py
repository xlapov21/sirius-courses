from hlapov_egor.lib.db_connector import db_connector


def init():
    query_for_users = """
CREATE TABLE users(
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            chat_id TEXT,
            name TEXT,
            user_name TEXT
);
"""
    query_for_answers = """

CREATE TABLE answers(
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            description TEXT
);
"""
    query_for_questions = """
CREATE TABLE questions(
            id INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT,
            description TEXT,
            answer_id INTEGER,
            FOREIGN KEY (answer_id)  REFERENCES answers (id)
);
"""
    db_connector.exec(query_for_users)
    db_connector.exec(query_for_answers)
    db_connector.exec(query_for_questions)
