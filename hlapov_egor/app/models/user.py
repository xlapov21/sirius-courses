from hlapov_egor.lib.db_connector import db_connector


class User:

    def __init__(self, id, chat_id, name, user_name):
        self.id = id
        self.chat_id = chat_id
        self.name = name
        self.user_name = user_name

    @staticmethod
    def create(chat_id, name, user_name):

        query = f"""
INSERT INTO users (chat_id, name, user_name)
VALUES ('{chat_id}', '{name}', '{user_name}')
RETURNING id
        """
        return User.find(db_connector.exec(query)[0])

    @staticmethod
    def find(id):
        query = f"""
SELECT * 
FROM users
WHERE id = {id}
                """
        result = db_connector.exec(query)
        return User(result[0], result[1], result[2], result[3]) if result else None

    def update(self):
        ...

    def destroy(self):
        ...
