import hlapov_egor.app.services.sentence_analyzer as sentence_analyzer
import hlapov_egor.resources.db.questions_answers as qa

class Question():

    def __init__(self, text, answer):
        self.text = text
        self.answer = answer

    def get_text(self):
        return self.text

    def get_answer(self):
        return self.answer

    @staticmethod
    def find_by_message(message):
        near_question = sentence_analyzer.SentenceAnalyzer().find_near_question(message)
        return Question(near_question, qa.ANSWERS[qa.QUESTIONS[near_question]])

print(Question.find_by_message('Привет, хочу узнать погоду'))

