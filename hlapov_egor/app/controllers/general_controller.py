import hlapov_egor.resources.db.questions_answers as qa
from hlapov_egor.app.models.guestion import Question


def register_command(bot):

    @bot.message_handler(commands=['start'])
    def start(message):
        bot.send_message(message.chat.id, 'Привет')

    @bot.message_handler(commands=['questions'])
    def questions(message):
        bot.send_message(message.chat.id, list(qa.QUESTIONS.keys()).join('\n'))

    @bot.message_handler(content_types=['text'])
    def send_answer(message):
        question = Question.find_by_message(message.text)
        bot.send_message(message.chat.id, question.get_answer())


