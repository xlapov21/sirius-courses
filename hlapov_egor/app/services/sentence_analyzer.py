from sentence_transformers import SentenceTransformer, util
import hlapov_egor.resources.db.questions_answers as qa
class SentenceAnalyzer():
    model = SentenceTransformer('all-MiniLM-L6-v2')
    questions = list(qa.QUESTIONS.keys())
    QUESTION_EMBEDDINGS = model.encode(questions)

    def __init__(self):
        if not SentenceAnalyzer.model:
            SentenceAnalyzer.model = SentenceTransformer('all-MiniLM-L6-v2')
            SentenceAnalyzer.QUESTION_EMBEDDINGS = SentenceAnalyzer.model.encode(SentenceAnalyzer.questions)

    @staticmethod
    def find_near_question(text):
        cos_sims = util.cos_sim(SentenceAnalyzer.QUESTION_EMBEDDINGS, [SentenceAnalyzer.model.encode(text)])
        sentence_combinations = []
        for i in range(0, len(cos_sims)):
            sentence_combinations.append([i, cos_sims[i]])
        sorted_questions = sorted(sentence_combinations, key=lambda x: x[1], reverse=True)
        return SentenceAnalyzer.questions[sorted_questions[0][0]]

# print(find_near_question('Привет, хочу узнать погоду'))