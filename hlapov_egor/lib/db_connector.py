import sqlite3 as sl


class DbConnector:

    def __init__(self):
        self.__con = sl.connect('my-test.db')
        self.cur = self.__con.cursor()

    def exec(self, text):
        self.cur.execute(text)
        return self.cur.fetchone()

db_connector = DbConnector()
